# Compact, colorized git log
alias gb="git branch"
alias gc="git checkout"
alias gf="git fetch"
alias gp="git pull"
alias gs="git status"
alias gpf="git reset --hard HEAD; git pull"
alias gl="git log --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
alias glg="git log --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --graph"
