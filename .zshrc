#!/usr/bin/env zsh
#!/bin/zsh
#              _              
#      _______| |__  _ __ ___ 
#     |_  / __| '_ \| '__/ __|
#    _ / /\__ \ | | | | | (__ 
#   (_)___|___/_| |_|_|  \___|
#

## Source basis files and plugins
#################################################
for file in $ZDOTDIR/conf/*.zsh
  do
  	source $file
 	echo -e "source:" $file
done

## Autoload zsh functions
#################################################
autoload -U colors && colors
autoload -Uz $fpath[-1]/*(.:t)


PROMPT="%{$fg[green]%}%n %{$fg[cyan]%}%(4~|%-1~/.../%2~|%~)%{$reset_color%}$ "

#
# zsh-history-substring-search
#

# Bind ^[[A/^[[B manually so up/down works both before and after zle-line-init
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
