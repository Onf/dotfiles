#!/usr/bin/env zsh
#!/bin/zsh
#          _ _                     
#     __ _| (_) __ _ ___  ___  ___ 
#    / _` | | |/ _` / __|/ _ \/ __|
#   | (_| | | | (_| \__ \  __/\__ \
#    \__,_|_|_|\__,_|___/\___||___/
# 

## LS
alias ls='ls --color=auto --group-directories-first -F'
alias ll='ls --time-style=+"%d.%m.%Y %H:%M" -l'
alias la='ll -avh'

## base operation
alias cp='cp -iv'       # Confirm before overwriting something
alias mv='mv -iv'       # Confirm before overwriting something
alias rm='rm -iv'       # Confirm before deleting anything

# Lists the ten most used commands.
alias history-stat="history 0 | awk '{print \$2}' | sort | uniq -c | sort -n -r | head"
