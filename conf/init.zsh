#!/usr/bin/env zsh
#!/bin/zsh
autoload -Uz $fpath[-1]/*(.:t)

MODULES=$$ZDOTDIR/modules

source $MODULES/git/aliases.zsh

source $MODULES/syntax-highlighting/zsh-syntax-highlighting.zsh
source $MODULES/history-substring-search/zsh-history-substring-search.zsh


# autoload -Uz git-alias-lookup git-branch-current git-branch-delete-interactive git-dir git-ignore-add git-root git-stash-clear-interactive git-stash-recover git-submodule-move git-submodule-remove mkcd mkpw duration-info-precmd duration-info-preexec coalesce git-action git-info

# source $$ZDOTDIR/modules/environment/init.zsh
# source $$ZDOTDIR/modules/git/init.zsh
# source $$ZDOTDIR/modules/input/init.zsh
# source $$ZDOTDIR/modules/termtitle/init.zsh
# source $$ZDOTDIR/modules/utility/init.zsh
# source $$ZDOTDIR/modules/duration-info/init.zsh
# source $$ZDOTDIR/modules/asciiship/asciiship.zsh-theme

# source $$ZDOTDIR/modules/completion/init.zsh
# source $$ZDOTDIR/modules/zsh-completions/zsh-completions.plugin.zsh
# source $$ZDOTDIR/modules/zsh-autosuggestions/zsh-autosuggestions.zsh
# source $$ZDOTDIR/modules/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# source $$ZDOTDIR/modules/zsh-history-substring-search/zsh-history-substring-search.zsh
