#!/usr/bin/env zsh
#!/bin/zsh
#              _                     
#      _______| |__   ___ _ ____   __
#     |_  / __| '_ \ / _ \ '_ \ \ / /
#    _ / /\__ \ | | |  __/ | | \ V / 
#   (_)___|___/_| |_|\___|_| |_|\_/  
#

# Set personal cache folder
export XDG_CACHE_HOME="$BUILDERS_HOME/.cache"
export MODULES=$ZDOTDIR/modules

fpath+=$ZDOTDIR/fn
# Set the list of directories that Zsh searches for programs.
path=(
    $HOME/.local/bin
    $HOME/.gem/ruby/2.6.0/bin
    $HOME/.config/composer/vendor/bin
    /usr/local/{bin,sbin}
    $path
)

# Ensure path arrays do not contain duplicates.
typeset -gU path fpath 

